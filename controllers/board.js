/**
 * Created by waca on 2016. 6. 22..
 */
'use strict'
var express = require('express')
    , router = express.Router();
var Board = require('../models/Board');
var fs = require('fs');


router.get('/',function(req,res,next){
	res.redirect('/')
});

router.get('/:board/:page/:search?',function(req,res,next){
	var limit = 6
	, total = 0
	, totalPage = 0
	, rowPage = 1 
	, curPage = req.params.page || 1
	, lastPage = 0;

	var search =req.params.search
	var param = {};
	if(search){
		param = {
			$or :[{
				heading : new RegExp(search,"i")
			}
			,{
				content : new RegExp(search,"i")
			}]
		}
	}
	Board.find(param).sort({content_id : -1}).skip((curPage-1)*limit).limit(limit).exec(function(err,content){
		if(err){
			return handleError(err);
		}else{
			if(content == '' || content == null){
				res.render('./board/list',{ title : '검색결과가 없습니다.'});
			}else{
				Board.count(param).count(function(err,data){
					//페이징
					total = data;
					
					if(total % limit >= 1){
						totalPage = parseInt((total/limit)+1);
					}else{
						totalPage = (total/limit);
					}
					
					for(var i = rowPage; i <= curPage ; i = i+5){
						rowPage = i;
					}
					
					lastPage=rowPage+5;
					for(var i =lastPage; i > totalPage ; i=i-1){
						lastPage = i;
					}
					res.render('./board/list',{item : content , total:total , totalPage :totalPage , curPage : curPage , rowPage : rowPage , lastPage : lastPage ,search : search ,title : '냠냠냠' });
				})
			}
		}
	});
});

router.get('/view/:id',function(req,res,next){
	res.render('./board/view')
});

router.get('/write',function(req,res,next){
	res.render('./board/write')
});

router.post('/write',function(req,res,next){
	new Board({
		heading : req.body.heading,
		content: req.body.content,
		author : req.user.name
	}).save(function() {

	});
	res.send('200');
});

router.get('/delete/:id',function (req,res,next) {
	res.redirect('/')
});

router.get('/update/:id',function (req,rest,next) {
	res.render('./board/write')
});

router.post('/update/:id',function (req,res,next) {
	res.redirect('/')
});

module.exports = router ;