var express = require('express');
var router = express.Router();

router.get('/',function(req, res) {
  res.render('home', {
    title: 'Home'
  });
});

router.get('/aboutme',function(req,res){
	res.render('/about/index');
});
module.exports = router;