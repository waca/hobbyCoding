/**
 * Created by waca on 2016. 6. 22..
 */
var express = require('express')
    , router = express.Router() ;

router.get('/:picture/:page',function(req,res,next){
	res.render('/picture/list');
});

router.get('/view/:id',function(req,res,next){
	res.render('/picture/view');
});

router.get('/write',function(req,res,next){
	res.render('/picture/write');
});

router.post('/write',function(req,res,next){
	res.redirect('/');
});

router.get('/delete/:id',function (req,res,next) {
	res.redirect('/');
});

router.get('/update/:id',function (req,rest,next) {
	res.render('/picture/write');
});

router.post('/update/:id',function (req,res,next) {
	res.redirect('/');
});

module.exports = router ;