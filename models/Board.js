/**
 * Created by waca on 2016. 6. 22..
 */
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var db = mongoose.connection;
autoIncrement.initialize(db);

var schemaOptions = {
  collection : 'boards',
  timestamps: true,
  toJSON: {
    virtuals: true
  }
};


var boardSchema = new mongoose.Schema({
	heading : String
	, content : String
	, imgPath : String
	, fileName : String
	, author : String
},schemaOptions);

boardSchema.plugin(autoIncrement.plugin,{
	model : 'boards'
	, field : 'content_id'
	, startAt : 1
});

boardSchema.pre('save',function(next){
	next();
});


var Board = mongoose.model('boards',boardSchema,'boards');
module.exports = Board;