//module
var express = require('express')
  , path = require('path')
  , logger = require('morgan')
  , compression = require('compression')
  , methodOverride = require('method-override')
  , session = require('express-session')
  , flash = require('express-flash')
  , bodyParser = require('body-parser')
  , expressValidator = require('express-validator')
  , dotenv = require('dotenv')
  , mongoose = require('mongoose')
  , passport = require('passport')
  , multer = require('multer')
  , autoIncrement = require('mongoose-auto-increment');
//use controller
var home = require('./controllers/home')
    , contact = require('./controllers/contact')
    , user = require('./controllers/user')
    , board = require('./controllers/board')
    , picture = require('./controllers/picture');

'use strict';

// Load environment variables from .env file
if(process.env.ONOFF !== 'on'){
	dotenv.load();
}

// Passport OAuth strategies
require('./config/passport');

var app = express();
app.locals.moment = require('moment');


mongoose.connect(process.env.MONGODB || 'http://localhost:27017/blog');
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
  process.exit(1);
});
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('port', process.env.PORT || 3000);
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(methodOverride('_method'));
app.use(session({ secret: process.env.SESSION_SECRET, resave: true, saveUninitialized: true }));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(multer({dest:'./upload/'}).single('img'));
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});
app.use(express.static(path.join(__dirname, 'public')));


app.use('/contact',function(req,res,next){
  if (req.isAuthenticated()) {
    next();
  } else {
    res.redirect('/login');
  }
});
app.use('/board',function(req,res,next){
	if (req.isAuthenticated()) {
		next();
	} else {
		res.redirect('/login');
	}
});
app.use('/',home);
app.use('/',contact);
app.use('/',user);
app.use('/board', board);
app.use('/picture',picture);

// Production error handler
if (app.get('env') === 'production') {
  app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.sendStatus(err.status || 500);
  });
}

app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + app.get('port'));
});

module.exports = app;
